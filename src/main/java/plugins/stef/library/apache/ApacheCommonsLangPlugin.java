package plugins.stef.library.apache;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Apache Commons Lang library.
 * 
 * @author Stephane Dallongeville
 */
public class ApacheCommonsLangPlugin extends Plugin implements PluginLibrary
{
    //
}
